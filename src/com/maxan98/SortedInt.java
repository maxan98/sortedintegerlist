package com.maxan98;

import javafx.collections.transformation.SortedList;

import java.util.*;

/**
 * Created by MaksSklyarov on 19.04.17.
 */
public class SortedInt {
   private LinkedList<Integer> list;
   private boolean repeat;
   private boolean side;



   public SortedInt(boolean ok,boolean side){
       list = new LinkedList<Integer>();
       this.repeat = ok;
       this.side = side;
   }
   public SortedInt combine(SortedInt l2){
       if(this.repeat!=l2.getrepeat()){
           throw new RuntimeException("Не совпадают типы списков");
       }
       SortedInt res = new SortedInt(this.getrepeat(),this.side);
       ListIterator<Integer> it = list.listIterator();
       //ListIterator<Integer> it2 = l2.getList().listIterator();
       while(it.hasNext()){
           ListIterator<Integer> it2 = l2.getList().listIterator(0);
           int tmp = it.next();
           int tmp2 = it2.next();
           while(tmp2<tmp){
               tmp2 = it2.next();

           }
           if(tmp2 == tmp){
               res.add(tmp);
           }
       }
       return res;
   }
   public boolean getrepeat(){return repeat;}

    public LinkedList<Integer> getList() {
        return list;
    }

    public void add(int a){
int temp;
ListIterator<Integer> it = list.listIterator();
if(list.size()!=0){
while(it.hasNext()){
    temp = it.next();
    if(temp == a && !repeat) {
        return;
    }   if(side){
        if(temp > a){
            it.previous();
            it.add(a);
            return;
    }}else if (!side){if(temp < a){
        it.previous();
        it.add(a);
        return;
    }}
}
    it.add(a);
}
    if(list.size()==0){
        list.add(a);
    }
}
public int get(int i){
        return list.get(i);
}
public int size(){
    return list.size();
}


   public void remove (int a){
ListIterator<Integer> it = list.listIterator();

while(it.hasNext()){
    int tmp = it.next();
    if(tmp == a){
        it.remove();
    }
}
   }

    @Override
    public String toString() {
        ListIterator i = list.listIterator();
        StringBuilder sb = new StringBuilder();
        while(i.hasNext()){
            sb.append("["+i.next()+ "] ");
        }
        return sb.toString();
    }
    @Override
    public boolean equals(Object aOtherObject) {
        if(aOtherObject == null) {
            return false;
        }

        if(this.getClass() != aOtherObject.getClass()) {
            return false;
        }

        if(this == aOtherObject) {
            return true;
        }

        SortedInt aOther = (SortedInt)aOtherObject;


        if(this.list.size() == aOther.list.size()) {
            ListIterator<Integer> litFirst = list.listIterator(0);
            ListIterator<Integer> litSecond = aOther.list.listIterator(0);

            while(litFirst.hasNext()) {
                if(litFirst.next() != litSecond.next()) {
                    return false;
                }
            }
        }
        else {
            return false;
        }

        return true;
    }
    public SortedInt reverse(){

       SortedInt si = new SortedInt(this.repeat,!this.side);
       ListIterator<Integer> it = list.listIterator();
       while(it.hasNext()){

           si.add(it.next());

       }
       return si;
    }
}
